from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from core.views import LoginView
from core.views import DashboardView
from core.views import ProximamenteView
from core.views.login import LogoutView
from core.views.reservoir import ReservoirView
from core.views.project import ProjectListView, ProjectFormView, DeleteProjectView
from core.views.simulation import InitialConditionsFormView, SimulationConditionsFormView
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
lr = login_required

urlpatterns = patterns('',
    url(r'^$', lr(DashboardView.as_view()), name='dashboard'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(url='/login'), name='logout'),
    url(r'^dashboard/$', lr(DashboardView.as_view()), name='dashboard'),
    url(r'^projects/$', lr(ProjectListView.as_view()), name='project_list'),
    url(r'^projects/add/$', lr(ProjectFormView.as_view()), name='add_project'),
    url(r'^projects/(?P<project_id>[0-9])/edit/$', lr(ProjectFormView.as_view()), name='edit_project'),
    url(r'^projects/(?P<project_id>[0-9])/delete/$', lr(DeleteProjectView.as_view()), name='delete_project'),
    url(r'^proximamente/$', ProximamenteView.as_view(), name='proximamente'),
    url(r'^reservoir/add$', lr(ReservoirView.as_view()), name='add_reservoir'),
    url(r'^reservoir/(?P<reservoir_id>[0-9])/edit/$', lr(ReservoirView.as_view()), name='edit_reservoir'),

    url(r'^initial_conditions/add$',lr(InitialConditionsFormView.as_view()), name='add_initialcond'),
    url(r'^initialcond/(?P<initialcond_id>[0-9])/edit/$', lr(InitialConditionsFormView.as_view()), name='edit_initialcond'),

    url(r'^simulation_conditions/add$',lr(SimulationConditionsFormView.as_view()), name='add_simulationcond'),
    url(r'^simulationcond/(?P<simulationcond_id>[0-9])/edit/$', lr(SimulationConditionsFormView.as_view()), name='edit_simulationcond'),

    # Examples:
    # url(r'^$', 'midf.views.home', name='home'),
    # url(r'^midf/', include('midf.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
