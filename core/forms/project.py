from django import forms
from core.models import Project

EMPTY_CHOICE = ('', '-----')
OPT_NONE, OPT_EQUALS, OPT_CONTAINS = '','1', '2'

class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project


class ProjectSearchForm(forms.Form):
    name_filter =  forms.ChoiceField(choices=(EMPTY_CHOICE, (OPT_EQUALS,'Equals'), (OPT_CONTAINS, 'Contains')), required=False)
    name_value =  forms.CharField(required=False)

    date_from = forms.DateField(required=False)
    date_to = forms.DateField(required=False)
    description_filter =  forms.ChoiceField(choices=(EMPTY_CHOICE, (OPT_CONTAINS,'Contains')), required=False)
    description_value = forms.CharField(required=False, max_length=100, label='Description')


    def get_projects(self):
        project_basequeryset = Project.objects.all()
        if not self.is_bound: # form hasn't been delivered yet
            return project_basequeryset
        if self.is_valid():
            #Filter data
            pname_filter  = self.cleaned_data['name_filter']
            pname_value = self.cleaned_data['name_value']
            description_filter  = self.cleaned_data['description_filter']
            description_value = self.cleaned_data['description_value']
            date_from = self.cleaned_data['date_from']
            date_to = self.cleaned_data['date_to']

            # Filter queryset
            project_qset = project_basequeryset # Base query

            # Filter by project name
            #pdb.set_trace()
            if pname_filter == OPT_EQUALS:
                project_qset = project_qset.filter(name=pname_value)
            elif pname_filter == OPT_CONTAINS:
                project_qset = project_qset.filter(name__contains=pname_value)

            # Filter by  description
            if description_filter == OPT_EQUALS:
                project_qset = project_qset.filter(description=description_value)
            elif description_filter == OPT_CONTAINS:
                project_qset = project_qset.filter(description__contains=description_value)
            #Filter by date
            if date_from is not None and date_to is not  None:
                project_qset = project_qset.filter(date__range=[date_from, date_to])
            elif date_from is not None:
                project_qset = project_qset.filter(date__gt=date_from) #greater than
            elif date_to is not None:
                project_qset = project_qset.filter(date__lt=date_to) #less than

            return project_qset
        else:
            return Project.objects.none()
        return project_basequeryset
        

    '''Validators'''

    def clean_name_value(self):
        value = self.cleaned_data['name_value']
        selected_filter  = self.cleaned_data['name_filter']

        if selected_filter != OPT_NONE and value.strip() == '':
            raise forms.ValidationError(" Please input value for selected name filter")
        elif selected_filter == OPT_NONE and value.strip() != '':
            raise forms.ValidationError(" Please select a filter type for name value")
        return value

    def clean_description_value(self):
        value = self.cleaned_data['description_value']
        selected_filter  = self.cleaned_data['description_filter']

        if selected_filter != OPT_NONE and value.strip() == '':
            raise forms.ValidationError(" Please input value for selected description filter")
        elif selected_filter == OPT_NONE and value.strip() != '':
            raise forms.ValidationError(" Please select a filter type for description value")
        return value

    def clean_date_to(self):
        date_from = self.cleaned_data['date_from']
        date_to = self.cleaned_data['date_to']
        if date_from is None and date_to is None:
            return None

        if date_from is not None and date_to is not None:
            if date_to < date_from:
                raise forms.ValidationError(" Date interval error (to < from)")

        return date_to
