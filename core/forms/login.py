from django.contrib.auth.forms import AuthenticationForm
from django import forms
import re


class LoginForm(AuthenticationForm):
    pat_reg = re.compile('^[a-zA-Z0-9_]+$')

    def clean_username(self):
        username = self.cleaned_data['username']

        if self.validate_special_chars(username):
            return username

    def clean_password(self):
        password = self.cleaned_data['password']
        if self.validate_special_chars(password):
            return password

    def validate_special_chars(self, value):
        if not self.pat_reg.match(value):
            raise forms.ValidationError(
                "Este campo no puede contener caracteres especiales #$%&/..., etc.")

        return True
