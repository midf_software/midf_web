from django import forms
from core.models import Reservoir

class ReservoirForm(forms.ModelForm):
    
    class Meta:
        model = Reservoir