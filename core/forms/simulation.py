from django import forms
from core.models import Simulation, SimulationConditions, InitialConditions


class SimulationForm(forms.ModelForm):

    class Meta:
        fields = ('name', )
        model = Simulation


class SimulationConditionsForm(forms.ModelForm):

    class Meta:
        model = SimulationConditions
        exclude = ('simulation',)


class InitialConditionsForm(forms.ModelForm):

    class Meta:
        model = InitialConditions
        exclude = ('simulation',)
