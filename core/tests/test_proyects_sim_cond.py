"""
This file contains the required test cases for the simulation conditions data form.
"""

from django.test import TestCase, LiveServerTestCase
from splinter import Browser
from core.tests.base import BaseSplinterTestCase


class SimConditionsTest(BaseSplinterTestCase):
    #browser = Browser()
    #url = 'http://127.0.0.1:8000/'

    """ Pruebas para el formulario de condiciones iniciales de simulacion"""



    def setUp(self):
        super(SimConditionsTest, self).setUp()
        sim_id = 1 #self.simulation.pk
        self.browser.visit(self.url + '/simulation_conditions/add?sim_id=%s'%(sim_id))
        self.radial_numblocks = self.browser.find_by_id('id_radial_numblocks')
        self.vertical_numblocks = self.browser.find_by_id('id_vertical_numblocks')
        self.max_timestep = self.browser.find_by_id('id_max_timestep')
        self.min_timestep = self.browser.find_by_id('id_min_timestep')
        self.total_time = self.browser.find_by_id('id_total_time')
        self.max_pressure_delta = self.browser.find_by_id('id_max_pressure_delta')
        self.max_saturation_delta = self.browser.find_by_id('id_max_saturation_delta')
        self.max_rs_delta = self.browser.find_by_id('id_max_rs_delta')
        self.dt_accel_factor = self.browser.find_by_id('id_dt_accel_factor')
        self.relative_error = self.browser.find_by_id('id_relative_error')
        self.epsilon = self.browser.find_by_id('id_epsilon')
        self.num_iterations = self.browser.find_by_id('id_num_iterations')
        
        self.clean_button = self.browser.find_by_id('clean_btn')
        self.save_button = self.browser.find_by_id('save_btn')
        self.next_button = self.browser.find_by_id('next_btn')
        self.prev_button = self.browser.find_by_id('prev_btn')
    

    def fill_fields(self, flag):
        """ llena los campos exceptuando el que tenga el flag """
        if flag != 0:
            self.radial_numblocks.fill(0)
        if flag != 1:
            self.vertical_numblocks.fill(0)
        if flag != 2:
            self.max_timestep.fill(0)
        if flag != 3:
            self.min_timestep.fill(0)
        if flag != 4:
            self.total_time.fill(0)
        if flag != 5:
            self.max_pressure_delta.fill(0)
        if flag != 6:
            self.max_saturation_delta.fill(0)
        if flag != 7:
            self.max_rs_delta.fill(0)
        if flag != 8:
            self.dt_accel_factor.fill(0)
        if flag != 9:
            self.relative_error.fill(0)
        if flag != 10:
            self.epsilon.fill(0)
        if flag != 11:
            self.num_iterations.fill(0)
            
    def test_empty_radial_numblocks(self):
        
        self.fill_fields(0)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_radial_numblocks_field(self):
        
        self.fill_fields(0)
        self.radial_numblocks.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_empty_vertical_numblocks(self):
        
        self.fill_fields(1)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_vertical_numblocks(self):
        
        self.fill_fields(1)
        self.vertical_numblocks.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_max_timestep(self):
        
        self.fill_fields(2)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_max_timestep(self):
        
        self.fill_fields(2)
        self.max_timestep.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_min_timestep(self):
        
        self.fill_fields(3)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_min_timestep(self):
        
        self.fill_fields(3)
        self.min_timestep.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_total_time(self):
        
        self.fill_fields(4)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_total_time(self):
        
        self.fill_fields(4)
        self.total_time.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_max_pressure_delta(self):
        
        self.fill_fields(5)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_max_pressure_delta(self):
        
        self.fill_fields(5)
        self.max_pressure_delta.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_max_saturation_delta(self):
        
        self.fill_fields(6)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_max_saturation_delta(self):
        
        self.fill_fields(6)
        self.max_saturation_delta.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_max_rs_delta(self):
        
        self.fill_fields(7)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_max_rs_delta(self):
        
        self.fill_fields(7)
        self.max_rs_delta.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_dt_accel_factor(self):
        
        self.fill_fields(8)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_dt_accel_factor(self):
        
        self.fill_fields(8)
        self.dt_accel_factor.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_relative_error(self):
        
        self.fill_fields(9)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_relative_error(self):
        
        self.fill_fields(9)
        self.relative_error.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_epsilon(self):
        
        self.fill_fields(10)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_epsilon(self):
        
        self.fill_fields(10)
        self.epsilon.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_empty_num_iterations(self):
        
        self.fill_fields(11)
        self.save_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_num_iterations(self):
        
        self.fill_fields(11)
        self.num_iterations.fill("a")
        self.save_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
                    
    def test_cleaning_data(self):
        self.fill_fields(999)
        self.clean_button.click()
        
        self.assertEqual(self.radial_numblocks.value, '')
        self.assertEqual(self.vertical_numblocks.value, '')
        self.assertEqual(self.max_timestep.value, '')
        self.assertEqual(self.min_timestep.value, '')
        self.assertEqual(self.total_time.value, '')
        self.assertEqual(self.max_pressure_delta.value, '')
        self.assertEqual(self.max_saturation_delta.value, '')
        self.assertEqual(self.max_rs_delta.value, '')
        self.assertEqual(self.dt_accel_factor.value, '')
        self.assertEqual(self.relative_error.value, '')
        self.assertEqual(self.epsilon.value, '')
        self.assertEqual(self.num_iterations.value, '')
        
    def test_saving_data(self):
        self.fill_fields(999)
        self.save_button.click()
        if self.browser.is_text_present('Information saved successfully'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
