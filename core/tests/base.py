from core.models import Project, Simulation
from django.contrib.auth.models import User
from django.test import TestCase, LiveServerTestCase

from splinter import Browser

class BaseSplinterTestCase(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        cls.browser = Browser()
        super(BaseSplinterTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(BaseSplinterTestCase, cls).tearDownClass()

    def setUp(self):
        #Create user a test project and simulation
        self.user = User.objects.create_user('admin', 'admin@thebeatles.com', 'admin')
        self.user.save()
        self.project = Project(name='test_project')
        self.project.save()
        self.simulation = self.project.create_base_simulation()
        self.simulation.save()
        self.url = self.live_server_url
        self.splinter_login() # Login user

    def splinter_login(self):
        #Login user
        self.browser.visit(self.url + '/login')
        self.username_input = self.browser.find_by_id('id_username')
        self.password_input = self.browser.find_by_id('id_password')
        self.submit_button = self.browser.find_by_id('submit_login')
        self.username_input.fill('admin')
        self.password_input.fill('admin')
        self.submit_button.click()
