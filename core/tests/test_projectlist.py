from django.test import TestCase, LiveServerTestCase
from splinter import Browser
from core.tests.base import BaseSplinterTestCase


class ProjectListTest(BaseSplinterTestCase):
    #browser = Browser()
    #url = 'http://127.0.0.1:8000/'

    """ Pruebas para el formulario de listar proyectos """



    def setUp(self):
        super(ProjectListTest, self).setUp()
        self.browser.visit(self.url + '/projects/')
        fid = self.browser.find_by_id
        self.clean_button = self.browser.find_by_id('clean_btn')
        self.submit_button = self.browser.find_by_id('submit_btn')
        self.name_filter_field = fid('id_name_filter')
        self.name_value_field = fid('id_name_value')
        self.desc_filter_field = fid('id_description_filter')
        self.desc_value_field = fid('id_description_value')
        self.date_from_field = fid('id_date_from')
        self.date_to_field = fid('id_date_to')

    
    def test_name_field(self):
        self.name_filter_field.select("2") 
        self.name_value_field.fill("project") 
        self.submit_button.click()
        
        if not self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_name_field_invalid(self):
        
        self.name_value_field.fill("project") 
        self.submit_button.click()
        
        if self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_desc_field(self):
        
        self.desc_filter_field.select("2") 
        self.desc_value_field.fill("description") 
        self.submit_button.click()
        
        if not self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_desc_field_invalid(self):
        
        self.desc_value_field.fill("description") 
        self.submit_button.click()
        
        if self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)          

    def test_daterange_field(self):
        self.date_from_field.fill("07/01/2014") 
        self.date_to_field.fill("07/06/2014") 
        self.submit_button.click()
        
        if not self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_daterange_field_invalid(self):
        self.date_to_field.fill("07/01/2014") 
        self.date_from_field.fill("07/06/2014") 
        self.submit_button.click()
        
        if self.browser.is_text_present('Error'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_cleaning_data(self):
        self.clean_button.click()
        
        self.assertEqual(self.desc_value_field.value, '')
        self.assertEqual(self.name_value_field.value, '')
        self.assertEqual(self.date_from_field.value, '')
        self.assertEqual(self.date_to_field.value, '')
        
    def test_submit_data(self):
        self.name_filter_field.select("2") 
        self.name_value_field.fill("project") 
        self.date_from_field.fill("07/01/2014") 
        self.date_to_field.fill("07/06/2014") 
        self.submit_button.click()
        if self.browser.is_text_present('project'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
