"""
this file contains the required test cases for dashboard GUI.
"""

from django.test import TestCase
from splinter import Browser

class DashboardTest(TestCase):

    """ Pruebas para el Dashboard """

    url = "http://127.0.0.1:8000"  # TODO get from settings
    browser = Browser()

    def setUp(self):
        self.browser.visit(self.url + '/dashboard')
        self.projects_button = self.browser.find_by_id('project_link')
        self.parameters_button = self.browser.find_by_id('parameters_link')
        self.settings_button = self.browser.find_by_id('settings_link')
        self.logout_button = self.browser.find_by_id('logout_link')

    def test_boton_proyectos(self):
        self.projects_button.click()
        self.assertTrue('projects' in self.browser.url)

    def test_ingreso(self):
        self.parameters_button.click()
        self.assertTrue('proximamente' in self.browser.url)
        
    def test_boton_configuracion(self):
        self.settings_button.click()
        self.assertTrue('proximamente' in self.browser.url)

    def test_boton_logout(self):
        self.logout_button.click()
        self.assertTrue('login' in self.browser.url)
