"""
This file contains the required test cases for the reservoir data form.
"""

from django.test import TestCase
from splinter import Browser
from core.tests.base import BaseSplinterTestCase

class ReservoirTest(BaseSplinterTestCase):

    """ Pruebas para el formulario de yacimiento """


    def setUp(self):
        super(ReservoirTest, self).setUp()
        sim_id = self.simulation.pk
        self.browser.visit(self.url + '/reservoir/add?sim_id=%s'%(sim_id))
        self.thickness_field = self.browser.find_by_id('id_formation_thickness')
        self.wellbore_radius_field = self.browser.find_by_id('id_wellbore_radius')
        self.external_radius_field = self.browser.find_by_id('id_external_radius')
        self.top_depth_field = self.browser.find_by_id('id_formation_depth_top')
        self.permeability_radial = self.browser.find_by_id('id_radial_permeability')
        self.permeability_vertical = self.browser.find_by_id('id_vertical_permeability')
        self.porosity_field = self.browser.find_by_id('id_porosity')
        self.rock_density_field = self.browser.find_by_id('id_rock_density')
        self.clean_button = self.browser.find_by_id('clean_btn')
        self.save_button = self.browser.find_by_id('save_btn')
        self.next_button = self.browser.find_by_id('next_btn')
        self.prev_button = self.browser.find_by_id('prev_btn')
        self.parameters_button = self.save_button

    def fill_fields(self, flag):
        """ llena los campos exceptuando el que tenga el flag """
        if flag != 0:
            self.thickness_field.fill(0)
        if flag != 1:
            self.wellbore_radius_field.fill(0)
        if flag != 2:
            self.external_radius_field.fill(0)
        if flag != 3:
            self.top_depth_field.fill(0)
        if flag != 4:
            self.permeability_radial.fill(0)
        if flag != 5:
            self.permeability_vertical.fill(0)
        if flag != 6:
            self.porosity_field.fill(0)
        if flag != 7:
            self.rock_density_field.fill(0)
        
    def test_empty_thickness_field(self):
        
        self.fill_fields(0)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_thickness_field(self):
        
        self.fill_fields(0)
        self.thickness_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_empty_wellbore_radius_field(self):
        
        self.fill_fields(1)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_wellbore_radius_field(self):
        
        self.fill_fields(1)
        self.wellbore_radius_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_external_radius_field(self):
        
        self.fill_fields(2)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_external_radius_field(self):
        
        self.fill_fields(2)
        self.external_radius_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_top_depth_field(self):
        
        self.fill_fields(3)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_top_depth_field(self):
        
        self.fill_fields(3)
        self.top_depth_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_permeability_radial(self):
        
        self.fill_fields(4)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_permeability_radial(self):
        
        self.fill_fields(4)
        self.permeability_radial.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_permeability_vertical(self):
        
        self.fill_fields(5)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_permeability_vertical(self):
        
        self.fill_fields(5)
        self.permeability_vertical.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_porosity_field(self):
        
        self.fill_fields(6)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_porosity_field(self):
        
        self.fill_fields(6)
        self.porosity_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_rock_density_field(self):
        
        self.fill_fields(7)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_rock_density_field(self):
        
        self.fill_fields(7)
        self.rock_density_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_cleaning_data(self):
        self.fill_fields(9)
        self.clean_button.click()
        
        self.assertEqual(self.thickness_field.value, '')
        self.assertEqual(self.wellbore_radius_field.value, '')
        self.assertEqual(self.external_radius_field.value, '')
        self.assertEqual(self.top_depth_field.value, '')
        self.assertEqual(self.permeability_radial.value, '')
        self.assertEqual(self.permeability_vertical.value, '')
        self.assertEqual(self.porosity_field.value, '')
        self.assertEqual(self.rock_density_field.value, '')

    def test_saving_data(self):
        self.fill_fields(9)
        self.save_button.click()
        if self.browser.is_text_present('Information saved successfully'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_next_btn(self):
        
        self.fill_fields(9)
        self.next_button.click()
        self.assertTrue('initial_conditions' in self.browser.url)
        
    def test_prev_btn(self):
        
        self.fill_fields(9)
        self.prev_button.click()
        self.assertTrue('projects' in self.browser.url)
