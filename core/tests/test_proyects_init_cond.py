"""
This file contains the required test cases for the initioal conditions data form.
"""

from django.test import TestCase, LiveServerTestCase
from splinter import Browser
from core.tests.base import BaseSplinterTestCase


class InitConditionsTest(BaseSplinterTestCase):
    #browser = Browser()
    #url = 'http://127.0.0.1:8000/'

    """ Pruebas para el formulario de condiciones iniciales """



    def setUp(self):
        super(InitConditionsTest, self).setUp()
        sim_id = 1 #self.simulation.pk
        self.browser.visit(self.url + '/initial_conditions/add?sim_id=%s'%(sim_id))
        self.reservoir_weight_field = self.browser.find_by_id('id_initial_reservoir_weight')
        self.gas_saturation_field = self.browser.find_by_id('id_initial_gas_saturation')
        self.water_saturation_field = self.browser.find_by_id('id_initial_water_saturation')
        self.clean_button = self.browser.find_by_id('clean_btn')
        self.save_button = self.browser.find_by_id('save_btn')
        self.parameters_button = self.save_button
        self.next_button = self.browser.find_by_id('next_btn')
        self.prev_button = self.browser.find_by_id('prev_btn')
    

    def fill_fields(self, flag):
        """ llena los campos exceptuando el que tenga el flag """
        if flag != 0:
            self.reservoir_weight_field.fill(0)
        if flag != 1:
            self.gas_saturation_field.fill(0)
        if flag != 2:
            self.water_saturation_field.fill(0)
            
    def test_empty_reservoir_weight_field(self):
        
        self.fill_fields(0)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_reservoir_weight_field(self):
        
        self.fill_fields(0)
        self.reservoir_weight_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def test_empty_gas_saturation_field(self):
        
        self.fill_fields(1)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_gas_saturation_field(self):
        
        self.fill_fields(1)
        self.gas_saturation_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    def test_empty_water_saturation_field(self):
        
        self.fill_fields(2)
        self.parameters_button.click()
        
        if self.browser.is_text_present('This field is required.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
    
    def test_number_water_saturation_field(self):
        
        self.fill_fields(2)
        self.water_saturation_field.fill("a")
        self.parameters_button.click()
        
        if self.browser.is_text_present('Enter a number.'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
                    
    def test_cleaning_data(self):
        self.fill_fields(9)
        self.clean_button.click()
        
        self.assertEqual(self.reservoir_weight_field.value, '')
        self.assertEqual(self.gas_saturation_field.value, '')
        self.assertEqual(self.water_saturation_field.value, '')
        
    def test_saving_data(self):
        self.fill_fields(9)
        self.save_button.click()
        if self.browser.is_text_present('Information saved successfully'):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
            
    '''
    def test_next_btn(self):
        
        self.fill_fields(9)
        self.next_button.click()
        # TODO: completar cuando se desarrolle la sgte vista
        #self.assertTrue('initial_conditions' in self.browser.url)
        
    '''
    def test_prev_btn(self):
        
        self.fill_fields(9)
        self.prev_button.click()
        self.assertTrue('reservoir' in self.browser.url)
