"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from splinter import Browser
from core.tests.base import BaseSplinterTestCase

class SimpleTest(TestCase):

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)


class LoginTest(BaseSplinterTestCase):

    """ Pruebas para el sistema de Login """

    # Text substrings contained in error messages in rendered webpage
    # TODO get from settings
    especial_text = 'especiales'
    error_text = 'Error'

    def setUp(self):
        super(LoginTest, self).setUp()
        self.browser.visit(self.url + '/login')
        self.username_input = self.browser.find_by_id('id_username')
        self.password_input = self.browser.find_by_id('id_password')
        self.clean_button = self.browser.find_by_id('clean_login')
        self.submit_button = self.browser.find_by_id('submit_login')

    def test_field_user_cleaning(self):
        self.username_input.fill("melanie")
        self.clean_button.click()
        self.assertEqual(self.username_input.value, '')

    def test_field_password_cleaning(self):
        self.password_input.fill("password")
        self.clean_button.click()
        self.assertEqual(self.password_input.value, '')

    def test_field_empty_user(self):
        username = ''
        password = 'validpass'
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(self.browser.is_text_present('Error'))

    def test_field_alphanumeric_user(self):
        username = 'validusername'
        password = 'validpass'
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(not self.browser.is_text_present(self.especial_text))

    def test_field_especialchar_user(self):
        username = 'abcdef&-_'
        password = 'validpass'
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(self.browser.is_text_present(self.especial_text))

    def test_field_empty_password(self):
        username = 'melanie'
        password = ''
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(self.browser.is_text_present('Error'))

    def test_field_alphanumeric_password(self):
        username = 'melanie'
        password = 'alphanumericpass'
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(not self.browser.is_text_present(self.especial_text))

    def test_field_especialchar_password(self):
        username = 'melanie'
        password = 'special$%%&/'
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue(self.browser.is_text_present(self.error_text))

    def test_login(self):
        # TODO crear usuario pepito
        username = "admin"
        password = "admin"
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue('dashboard' in self.browser.url)

    def test_login_incorrect(self):
        # TODO crear usuario pepito
        username = "admin"
        password = "admin_wrong"
        self.fill_fields(username, password)
        self.submit_button.click()
        self.assertTrue('dashboard' not in self.browser.url)
        self.assertTrue(self.browser.is_text_present('Error'))

    def fill_fields(self, username, password):
        """ auxiliary method to fill username and password fields """
        self.username_input.fill(username)
        self.password_input.fill(password)
