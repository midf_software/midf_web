# -*- coding: utf-8 -*-
from django.db import models

# TODO: dividir en clases los modelos
# TODO: definir tipos de datos para los campos

# Create your models here.


class Project(models.Model):
    name = models.CharField("Project name", max_length=80)
    ref = models.CharField("Reference", max_length=30, default='', blank=True)
    description = models.TextField("Description", default='', blank=True)
    date = models.DateField("Date", auto_now_add=True)
    
    def create_base_simulation(self):
        simulations = self.simulation_set.all()
        if len(simulations):
            return simulations[0]
        else:
            base_sim = Simulation(project=self)
            base_sim.save()
            return base_sim
    
    def __unicode__(self):
        return "%s - %s"%(self.pk, self.name)

class Simulation(models.Model):
    name = models.CharField("Simulation", max_length=80, default='')
    # ref = models.CharField("Reference", max_length=30, default='', blank=True) # TODO necesario?
    #description = models.TextField("Description", default='', blank=True)
    date = models.DateField("Date", auto_now_add=True)
    project = models.ForeignKey(Project)
    reservoir = models.ForeignKey('Reservoir', null=True)

    def get_initial_conditions(self):
       try:
           return self.initial_conditions
       except:
           return None

    def get_simulation_conditions(self):
       try:
           return self.simulation_conditions
       except:
           return None

class Reservoir(models.Model):
    formation_thickness = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Formation thickness (ft)")
    external_radius = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="External radius (ft)")
    wellbore_radius = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Wellbore radius (ft)")
    formation_depth_top = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Formation top depth (ft)")
    radial_permeability = models.DecimalField(
            max_digits=12, decimal_places=4, verbose_name="Permeability: Radial (mD)")
    vertical_permeability = models.DecimalField(
            max_digits=12, decimal_places=4, verbose_name="Permeability: Vertical (mD)")
    porosity = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Porosity")
    rock_density = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Rock density (kg/m3)")


class InitialConditions(models.Model):
    simulation = models.OneToOneField(Simulation, related_name='initial_conditions', null=True)
    initial_reservoir_weight = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Initial Reservoir weight (psi)")
    initial_gas_saturation = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Initial Gas Saturation")
    initial_water_saturation = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Initial Water Saturation")


class SimulationConditions(models.Model):
    simulation = models.OneToOneField(Simulation, related_name='simulation_conditions', null=True)
    # No. De bloques en dirección radial
    radial_numblocks = models.IntegerField(
        verbose_name="Number of blocks. (Radial direction)", default=15)
    # No. De bloques en dirección vertical
    vertical_numblocks = models.IntegerField(
        verbose_name="Number of blocks. (Vertical direction)", default=3)
    max_timestep = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Max. time step (Dt - days)", default=0.01)  # Máximo time step
    min_timestep = models.DecimalField(
        max_digits=12, decimal_places=7, verbose_name="Min. time step (Dt - days)", default=0.000001)  # Minimo time step
    total_time = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Total time (days)", default=10)  # Tiempo total de simulación
    max_pressure_delta = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Max pressure delta (psi)", default=10)  # Delta de presión máximo permitido
    max_saturation_delta = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Mx saturation delta", default=0.05)  # Delta de saturación máximo permitido
    max_rs_delta = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Max Rs delta (SCF/STB)", default=100.0)  # Delta de Rs máximo permitido
    dt_accel_factor = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Dt acceleration factor", default=2.0)  # Factor para acelerar Dt
    relative_error = models.DecimalField(
        max_digits=12, decimal_places=8, verbose_name="Relative Error", default=0.00001)  # Error Relativo
    epsilon = models.DecimalField(
        max_digits=12, decimal_places=8, verbose_name="Epsilon", default=0.00001)  # Epsilon
    num_iterations = models.DecimalField(
        max_digits=12, decimal_places=4, verbose_name="Max iterations", default=20)  # Número máximo de iteraciones
