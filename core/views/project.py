from django.views.generic import ListView, TemplateView
from django.views.generic.edit import FormView
from core.models import Project, Simulation
from core.forms.project import ProjectForm
from core.views.base import BaseModelFormView
from core.forms.project import ProjectSearchForm
from django.contrib import messages 
from core import messages as msg


class ProjectListView(ListView):
    template_name = 'project_list.html'
    model = Project # TODO filter projects owned by user only?

    
    def dispatch(self, *args, **kwargs):
        self.search_form = ProjectSearchForm(self.request.POST or None)
        return super(ProjectListView, self).dispatch(*args, **kwargs)
    
    def post(self, request):
        return super(ProjectListView, self).get(request)

    def get_queryset(self):
        return self.search_form.get_projects()

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)
        context['search_form'] = self.search_form
        return context


class ProjectFormView(BaseModelFormView):
    template_name = 'project_form.html'
    success_url = 'edit_project'
    form_class = ProjectForm
    model = Project
    pk_url_kwarg = 'project_id'
    context_object_name = 'project'

    def form_valid(self, form):
        response = super(ProjectFormView, self).form_valid(form)
        # self.object is created inside super.form_valid
        project = self.object
        # Create default simulation only if project doesn't have one
        project.create_base_simulation()
        return response

    def get_context_data(self, **kwargs):
        context = super(ProjectFormView, self).get_context_data(**kwargs)
        if self.object is not None:
            # Reference simulation if project exists
            context['simulation'] = self.object.simulation_set.all()[0]
        return context


class DeleteProjectView(TemplateView):
    template_name = 'project_delete.html'
    
    def get(self, request, *args, **kwargs):
        project_id = self.kwargs['project_id']
        project = Project.objects.get(pk=project_id)
        project.delete()
        print "deleted"
        messages.add_message(self.request, messages.SUCCESS, msg.PROJECT_DELETE)
        return super(DeleteProjectView, self).get(request)
