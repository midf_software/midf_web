from django.views.generic import TemplateView
       

class ProximamenteView(TemplateView):

    template_name = 'proximamente.html'
    