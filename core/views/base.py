from django.views.generic.edit import UpdateView, ModelFormMixin, ProcessFormView
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from core.models import Reservoir, Project, Simulation
from django.contrib import messages 
from core import messages as msg

class BaseModelFormView(UpdateView):

    def get_object(self, queryset=None):
        if self.pk_url_kwarg in self.kwargs: # Edit
            return super(BaseModelFormView, self).get_object(queryset=queryset)
        else: # Add
            return None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(BaseModelFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(BaseModelFormView, self).post(request, *args, **kwargs)

    def form_valid(self, form, get_args=''):
        super(BaseModelFormView, self).form_valid(form)
        self.success_url = reverse(self.success_url, kwargs={self.pk_url_kwarg: self.object.pk}) + '?' + get_args
        messages.add_message(self.request, messages.SUCCESS, msg.FORM_SUCCESS)
        return HttpResponseRedirect(self.get_success_url())
    
    def form_invalid(self, form, get_args=''):
        messages.add_message(self.request, messages.ERROR, msg.FORM_ERROR)

        return super(BaseModelFormView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(BaseModelFormView, self).get_context_data(**kwargs)
        if self.object is None:
            context['mode'] = 'create'
        else:
            context['mode'] = 'update'
        return context


class BaseSimulationStepFormView(BaseModelFormView):
    simulation = None

    def form_valid(self, form):
        if 'sim_id' in self.request.GET:
            sim_id = self.request.GET['sim_id']
            response = super(BaseSimulationStepFormView, self).form_valid(form, 'sim_id=%s'%(sim_id)) # Add sim_id to preserve simulation context
            self.simulation = Simulation.objects.get(pk=sim_id)
        else:
            response = super(BaseSimulationStepFormView, self).form_valid(form)
        return response
    
    def get_context_data(self, **kwargs):
        context = super(BaseSimulationStepFormView, self).get_context_data(**kwargs)
        # Add relevant instances to template context
        if 'sim_id' in self.request.GET:
            simulation = Simulation.objects.get(pk=self.request.GET['sim_id'])
            project = simulation.project
            context['simulation'] = simulation
            context['project'] = project
        return context
