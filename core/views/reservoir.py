from django.views.generic.edit import FormView
from core.forms.reservoir import ReservoirForm
from core.views.base import BaseSimulationStepFormView
from core.models import Reservoir, Project, Simulation

class ReservoirView(BaseSimulationStepFormView):
    template_name = 'reservoir.html'
    success_url = 'edit_reservoir'
    form_class = ReservoirForm
    model = Reservoir
    pk_url_kwarg = 'reservoir_id'
    context_object_name = 'reservoir'
    
    def form_valid(self, form):
        response = super(ReservoirView, self).form_valid(form)
        if 'sim_id' in self.request.GET:
            #Add reservoir to simulation
            self.simulation.reservoir = self.object
            self.simulation.save()
        return response
