from django.views.generic.edit import FormView
from core.forms.simulation import (
    SimulationForm, SimulationConditionsForm, InitialConditionsForm)
from core.views.base import BaseSimulationStepFormView
from core.models import InitialConditions, SimulationConditions

class SimulationFormView(FormView):
    template_name = ''
    form_class = SimulationForm
    success_url = ''

    def form_valid(self, form):
        form.save(commit=True)
        return super(SimulationFormView, self).form_valid(form)


class SimulationConditionsFormView(BaseSimulationStepFormView):
    template_name = 'simulation_conditions_form.html'
    form_class = SimulationConditionsForm
    success_url = 'edit_simulationcond'
    model = SimulationConditions
    pk_url_kwarg = 'simulationcond_id'
    context_object_name = 'simulationcond'

    def form_valid(self, form):
        response = super(SimulationConditionsFormView, self).form_valid(form)
        if 'sim_id' in self.request.GET:
            # Add simulationconds to simulation
            self.simulation.simulation_conditions = self.object
            self.simulation.save()
        self.object.save()
        return response


class InitialConditionsFormView(BaseSimulationStepFormView):
    template_name = 'initial_conditions_form.html'
    success_url = 'edit_initialcond'
    form_class = InitialConditionsForm
    model = InitialConditions
    pk_url_kwarg = 'initialcond_id'
    context_object_name = 'initialcond'

    def form_valid(self, form):
        response = super(InitialConditionsFormView, self).form_valid(form)
        if 'sim_id' in self.request.GET:
            # Add initialconds to simulation
            self.simulation.initial_conditions = self.object
            self.simulation.save()
        self.object.save()
        return response
