from django.views.generic.edit import FormView
from django.views.generic import TemplateView, RedirectView
from core.forms.login import LoginForm
from django.contrib.auth import login, logout
       

class LoginView(FormView):

    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/dashboard/'

    def form_valid(self, form):
        user = form.user_cache
        login(self.request, user)
        return super(LoginView, self).form_valid(form)

class LogoutView(RedirectView):

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).dispatch(*args, **kwargs)
